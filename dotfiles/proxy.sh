#!/bin/bash

export http_proxy=http://proxy-ir.intel.com:911
export HTTP_PROXY=http://proxy-ir.intel.com:911
export https_proxy=http://proxy-ir.intel.com:911
export HTTPS_PROXY=http://proxy-ir.intel.com:911
export NO_PROXY=127.0.0.1,localhost,.sclab.intel.com,.intel.com,tapimages.us.enableiot.com
export no_proxy=$NO_PROXY
export _JAVA_OPTIONS="-Dhttp.proxyHost=proxy-mu.intel.com -Dhttp.proxyPort=911 -Dhttps.proxyHost=proxy-mu.intel.com -Dhttps.proxyPort=912 -Dhttp.nonProxyHosts=\"intel.com|*.intel.com|10.*|192.168.*|localhost|127.*\""

#!/bin/bash -x

export curdir=`dirname $0`

sudo redsocks -c ${curdir}/redsocks.conf

sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo sh -c "echo 1 > /proc/sys/net/ipv4/tcp_syncookies"
sudo sh -c "echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter"
sudo sh -c "echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts"
sudo sh -c "echo 1 > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses"

sudo iptables -t nat -N REDSOCKS

#sudo iptables -t nat -A REDSOCKS -m limit --limit 100/min -j LOG --log-prefix "REDSOCKS - BEGIN " --log-level 7
sudo iptables -t nat -A REDSOCKS -d 0.0.0.0/8 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 10.0.0.0/8 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 127.0.0.0/8 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 169.254.0.0/16 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 172.16.0.0/12 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 192.168.0.0/16 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 224.0.0.0/4 -j RETURN
sudo iptables -t nat -A REDSOCKS -d 240.0.0.0/4 -j RETURN
#sudo iptables -t nat -A REDSOCKS -m limit --limit 100/min -j LOG --log-prefix "REDSOCKS - WHITELIST SKIPPED " --log-level 7

sudo iptables -t nat -A REDSOCKS -p tcp -j REDIRECT --to-ports 12345
#sudo iptables -t nat -A REDSOCKS -m limit --limit 100/min -j LOG --log-prefix "REDSOCKS - SHOULD NOT HAPPEN " --log-level 7

#sudo iptables -t nat -A OUTPUT -m limit --limit 100/min -j LOG --log-prefix "OUTPUT - BEGIN " --log-level 7
sudo iptables -t nat -A OUTPUT -p tcp -j REDSOCKS

# DOCKER:
sudo iptables -t nat -A PREROUTING -i docker0 -p udp -j ACCEPT
#sudo iptables -t nat -A PREROUTING -i docker0 -p tcp -m limit --limit 100/min -j LOG --log-prefix "PREROUTING: " --log-level 7
sudo iptables -t nat -A PREROUTING -i docker0 -p tcp -j REDSOCKS
#sudo iptables -t nat -A PREROUTING -i docker0 -p tcp -m limit --limit 100/min -j LOG --log-prefix "SHOULD NOT HAPPEN: PREROUTING: " --log-level 7
